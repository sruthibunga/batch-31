def construct_golygons(cities):
    for city in cities:
        max_edge_length = city[0]
        blocked = city[1]
        golygons = []
        def backtrack(x, y, edge_length, direction, golygon):
            if (x, y) in blocked or edge_length > max_edge_length:
                return
            if x == 0 and y == 0:
                golygons.append(golygon)
                return
            if direction != "s":
                backtrack(x, y+1, edge_length+1, "n", golygon+"n")
            if direction != "n":
                backtrack(x, y-1, edge_length+1, "s", golygon+"s")
            if direction != "w":
                backtrack(x+1, y, edge_length+1, "e", golygon+"e")
            if direction != "e":
                backtrack(x-1, y, edge_length+1, "w", golygon+"w")
        backtrack(0, 0, 1, "", "")
        for golygon in golygons:
            print(golygon)
        print("Found {} golygon(s).".format(len(golygons)))

# Sample input
cities = [
    (8, {(-2, 0), (6, -2)}),
    (8, {(2, 1), (-2, 0)})
]

construct_golygons(cities)
